import React, { createContext, useContext, useState } from "react";
import classNames from "classnames";
import { Container, Body, Title, Text, Image, Button } from "./styles/Card";

const CardContext = createContext();

export const useCardContext = () => {
  const context = useContext(CardContext);

  if (!context)
    throw new Error(
      "Child components of Card cannot be rendered outside the Card component!"
    );

  return context;
};

export const Card = ({ classes, children, ...restProps }) => {
  const [toggled, setToggled] = useState(false);

  return (
    <CardContext.Provider value={{ toggled, setToggled }}>
      <Container className={classNames("card", classes)} {...restProps}>
        {children}
      </Container>
    </CardContext.Provider>
  );
};

const CardBody = ({ classes, children, ...restProps }) => {
  useCardContext();

  return (
    <Body className={classNames("card__body", classes)} {...restProps}>
      {children}
    </Body>
  );
};

const CardButton = ({ classes, children, ...restProps }) => {
  const { setToggled } = useCardContext();

  return (
    <Button
      className={classNames("card__button", classes)}
      {...restProps}
      onClick={() => setToggled((prev) => !prev)}
    >
      {children}
    </Button>
  );
};

const CardTitle = ({ classes, children, ...restProps }) => {
  return (
    <Title className={classNames("card__title", classes)} {...restProps}>
      {children}
    </Title>
  );
};

const CardText = ({ classes, children, ...restProps }) => {
  return (
    <Text className={classNames("card__text", classes)} {...restProps}>
      {children}
    </Text>
  );
};

const CardImage = ({ src, alt, classes, ...restProps }) => {
  return (
    <Image
      src={src}
      alt={alt}
      className={classNames("card__image", classes)}
      {...restProps}
    />
  );
};

Card.Button = CardButton;
Card.Body = CardBody;
Card.Image = CardImage;
Card.Text = CardText;
Card.Title = CardTitle;
