import React from "react";
import "./App.scss";
import { Card } from "./components";
import movies from "./data.json";

const App = () => {
  return (
    <div>
      {movies.map((movie) => (
        <Card className="mr" key={movie.id}>
          <Card.Image src={movie.image} alt={movie.title} />
          <Card.Body>
            <Card.Title>{movie.title}</Card.Title>
            <Card.Text>{movie.desc}</Card.Text>
            <Card.Button>{movie.title}</Card.Button>
          </Card.Body>
        </Card>
      ))}
    </div>
  );
};

export default App;
